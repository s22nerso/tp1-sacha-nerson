using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem;

public class RotateCube : MonoBehaviour
{
    // Start is called before the first frame update
    public float speed = 3.0f;
    public InputAction activateRotation;

    private bool rot = false;

    void Start()
    {
        activateRotation.Enable();
        activateRotation.performed += ctx => rot = !rot;
    }

    // Update is called once per frame
    void Update()
    {
        if (rot) {
            transform.Rotate(speed, speed, speed);
        }
    }
}
