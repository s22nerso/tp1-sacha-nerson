using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

public class CreateOnClick : MonoBehaviour, IPointerClickHandler
{
    public GameObject aCopier;

    public void OnPointerClick(PointerEventData eventData)
    {
        Instantiate(aCopier, Camera.main.transform.position, (0, 0, 0, 0));
        aCopier.GetComponent<Renderer>().material.color = Color.green;

        throw new System.NotImplementedException();
    }

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
