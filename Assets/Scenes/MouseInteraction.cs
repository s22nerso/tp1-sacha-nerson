using System.Collections;
using System.Collections.Generic;
using JetBrains.Annotations;
using UnityEngine;
using UnityEngine.EventSystems;


public class MouseInteraction : MonoBehaviour, IPointerClickHandler, IPointerEnterHandler, IPointerExitHandler
{
    private new Rigidbody rigidbody;
    private new Renderer renderer;
    private Color prevColor;

    public void OnPointerClick(PointerEventData eventData)
    {
        rigidbody.AddForce(Camera.main.transform.forward*200000f);
        throw new System.NotImplementedException();
    }

    public void OnPointerEnter(PointerEventData eventData)
    {
        renderer.material.color = Color.red;
        throw new System.NotImplementedException();
    }

    public void OnPointerExit(PointerEventData eventData)
    {
        renderer.material.color = prevColor;
        throw new System.NotImplementedException();
    }

    // Start is called before the first frame update
    void Start()
    {
        rigidbody = GetComponent<Rigidbody>();
        renderer = GetComponent<Renderer>();
        prevColor = renderer.material.color;
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
